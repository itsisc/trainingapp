const Sequelize = require('sequelize');

const database = new Sequelize(
    'training_devops_app',
    'cristi',
    'dudu',
    {
        dialect: "mysql",
        host: "localhost",
        dialectOptions: {
            charset: "utf8",
            collate: "utf8_general_ci",
        },
        define: {
            timestamps: true,
        },
    },
);

module.exports = database;
