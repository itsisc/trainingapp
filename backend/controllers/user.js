const {user: userDb} = require('../models');

const controller = {
    create: async (req, res) => {
        const payload = {
            name: req.body.name,
            numbers: req.body.numbers,
        }

        console.log(payload);

        try {
            if (payload.name === null || payload.name === undefined || payload.numbers === null || payload.numbers === undefined) {
                return res.status(400).send("Campurile nu au fost completate!");
            }
            const newUser = await userDb.create(payload);
            res.status(200).send(newUser);
        } catch (error) {
            res.status(500).send(error.message);
        }
    }
}

module.exports = controller