const express = require('express');
const cors = require('cors')
const port = 4004;

const app = express()

const {database} = require('./models')
const router = require('./routes');


app.use(express.json());

app.use(cors());


app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", `http://localhost:4004`);
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
});

app.use('/api', router);

app.get("/reset", function (req, res) {
    database.sync({force:true})
        .then(() => {
            res.status(200).send({message: "Baza de date a fost resetata cu succes"})
        })
        .catch((error) => {
            res.status(500).send(error)
        })
})

app.get('/', (req,res) => {
    res.status(200).send('Salut');
})

app.listen(port, () => {
    console.log(`App is running on port ${port}`);
})