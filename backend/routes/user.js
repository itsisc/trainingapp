const router = require('express').Router();

const {userController} = require('../controllers');

router.post('/create', userController.create);

module.exports = router;
