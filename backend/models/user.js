module.exports = (database, sequelize) => {
    return database.define('user', {
        id: {
            type: sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: sequelize.STRING,
            required: true
        },
        numbers: {
            type: sequelize.STRING,
            required: true
        },
    });
};
