const sequelize = require("sequelize");
const database = require("../config/database");
const userModel = require('./user');

const user = userModel(database, sequelize);

module.exports = {
    user,
    database
}
