const form = document.getElementById('form');

const handleSubmit = (e) => {
    e.preventDefault();
    const nameInput = document.getElementById('name');
    const numbersInput = document.getElementById('magic');
    const payload = {
        name: nameInput.value,
        numbers: numbersInput.value,
    }

    let isValid = true;

    if (!payload.name) {
        toastr.error("Nu ai introdus numele!");
        isValid = false;
    }

    if (!payload.numbers) {
        toastr.error("Nu ai introdus cele 3 cifre magice!");
        isValid = false;
    }

    if (isValid) {
        axios.post('http://localhost:4004/api/user/create', payload, {
            headers: {
                'Content-Type': 'application/json',
            }
        }).then((response) => {
            toastr(`Succes! Bine ai venit ${response.data.name}`);
            nameInput.value = '';
            numbersInput.value = '';
        }).catch((error) => {
            toastr(error);
            alert(error); //idk ce are ca nici asta nu merge
        })
    }
}

form.addEventListener('submit', handleSubmit);